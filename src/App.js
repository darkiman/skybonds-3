import React from 'react';
import styled from 'styled-components';
import BondCard from "./components/BondCard/BondCard";

const StyledAppContainer = styled.div`
    background-color: #101010;
    text-align: center;
    color: white;
    height: 100vh;
    overflow:auto;
    padding: 20px;
`;

function App() {
  return (
    <StyledAppContainer>
       <BondCard/>
    </StyledAppContainer>
  );
}

export default App;
