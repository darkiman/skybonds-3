import React from "react";
import styled from "styled-components";
import ChartView from "../Chart/ChartView";
import {useDispatch, useSelector} from "react-redux";
import {getBondCardData} from '../../components/Chart/ChartState';

const StyledBondContainer = styled.div`
    background-color: white;
    width: 500px;
    color: #101010;
	padding: 15px;
	text-align: left;
	margin: auto;
	min-height: 460px;
`;

const StyledHeader = styled.div`
    font-size: 30px;
    font-weight: bold;
`;

const StyledCurrency = styled.span`
    font-size: 18px;
    margin-left: 15px;
    font-weight: normal;
`;

const StyledDescription = styled.div`
	margin-top: 15px;
	margin-bottom: 15px;
`;

const StyledChartView = styled(ChartView)`
	margin-top: 15px;
	border-top: 1px solid #ccc;
`;

const StyledSpinner = styled.div`
  border: 16px solid #f3f3f3; /* Light grey */
  border-top: 16px solid #cccc; /* Blue */
  border-radius: 50%;
  width: 120px;
  height: 120px;
  animation: spin 2s linear infinite;
  margin: 140px auto 140px auto;
  
  @keyframes spin {
	  0% { transform: rotate(0deg); }
	  100% { transform: rotate(360deg); }
  }
`;


const BondCard = () => {
	const dispatch = useDispatch();
	const isLoading = useSelector(state => state.chart.isLoading);
	const info = useSelector(state => state.chart.info);

	React.useEffect(() => {
		dispatch(getBondCardData('random ISIN HERE'));
	}, []);

	return (
		<StyledBondContainer>
			{
				isLoading ? <StyledSpinner /> : <>
					<StyledHeader>
						{info.name}
						<StyledCurrency>{info.currency}</StyledCurrency>
					</StyledHeader>
					<StyledDescription>
						{info.label}
						<br/>
						{info.description}
					</StyledDescription>
					<StyledChartView></StyledChartView>
				</>
			}
		</StyledBondContainer>
	);
};

export default BondCard;
