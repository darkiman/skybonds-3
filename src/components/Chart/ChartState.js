import * as moment from 'moment';
import {getRandomData} from '../../utils/utils';

export const VIEW_TYPES = ['Yield', 'Spread', 'Price'];
export const PERIODS = ['Week', 'Month', 'Quarter', 'Year', 'Max'];
const DAYS_IN_WEEK = 7;
const DATE_FORMAT = 'YYYY.MM.DD';
const YEAR_FORMAT = 'YYYY';
const MONTHS_IN_QUARTER = 3;
const CURRENT_YEAR = moment().format(YEAR_FORMAT);

const getDataByPeriod = (chartData, period) => {
	let currentDay;
	let startDate;
	let endDate;
	let result;
	let data = chartData.data;
	switch (period) {
		case 'Week':
			currentDay = moment().isoWeekday();
			startDate = moment().subtract(currentDay, 'd');
			endDate = moment().add(DAYS_IN_WEEK - currentDay, 'd');
			result = data.filter(item => {
				const date = moment(item.x, DATE_FORMAT);
				return date.isSameOrAfter(startDate) && date.isSameOrBefore(endDate);
			});
			break;
		case 'Month':
			startDate = moment().startOf('month').format(DATE_FORMAT);
			endDate = moment().endOf("month").format(DATE_FORMAT);
			result = data.filter(item => {
				const date = moment(item.x, DATE_FORMAT);
				return date.isSameOrAfter(startDate) && date.isSameOrBefore(endDate);
			});
			break;
		case 'Quarter':
			const currentMonth = parseInt(moment(new Date()).format('M'));
			const startMonth = Math.floor(currentMonth / MONTHS_IN_QUARTER) * MONTHS_IN_QUARTER;
			const endMonth = startMonth + MONTHS_IN_QUARTER;
			startDate = moment().month(startMonth).date(1);
			endDate = moment().month(endMonth).endOf('month');
			result = data.filter(item => {
				const date = moment(item.x, DATE_FORMAT);
				return date.isSameOrAfter(startDate) && date.isSameOrBefore(endDate);
			});
			break;
		case 'Year':
			result = data.filter(item => {
				const year = moment(item.x, DATE_FORMAT).format(YEAR_FORMAT);
				return CURRENT_YEAR === year;
			});
			break;
		case 'Max':
			result = data;
			break;
		default:
			result = data;
	}
	return [{label: chartData.label, data: result}]
};

const initialState = {
	data: [],
	viewType: VIEW_TYPES[0],
	period: PERIODS[1],
	isLoading: false,
	periodData: [],
	info: {}
};

export const CHANGE_VIEW_TYPE = 'chart/CHANGE_VIEW_TYPE';
export const CHANGE_PERIOD = 'chart/CHANGE_PERIOD';

export const CHART_GET_DATA_REQUEST = 'chart/GET_DATA_REQUEST';
export const CHART_GET_DATA_SUCCESS = 'chart/GET_DATA_SUCCESS';
export const CHART_GET_DATA_FAILED = 'chart/GET_DATA_FAILED';

export const getBondCardData = (ISIN) => {
	return dispatch => {
		dispatch({
			type: CHART_GET_DATA_REQUEST,
		});
		new Promise((resolve) => {
			setTimeout(() => {
				resolve();
			}, 1000);
		}).then(() => {
			const data = VIEW_TYPES.map(item => {
				return {
					label: item,
					data: getRandomData()
				}
			});
			const info = {
				name: 'NII Capital 7.625 21',
				currency: 'USD',
				label: 'US67021BAE92',
				description: 'NII Capital COPR, Telecommunications, NR, till 01.04.2016'
			};
			const result = { data, info };
			dispatch({
				type: CHART_GET_DATA_SUCCESS,
				payload: result
			});
			return result;
		}).catch(error => {
			dispatch({
				type: CHART_GET_DATA_FAILED,
				payload: error
			});
			return error;
		});

	};
};


const ChartState = (state = initialState, action) => {
	switch (action.type) {

		case CHART_GET_DATA_REQUEST:
			return {
				...state,
				isLoading: true
			};

		case CHART_GET_DATA_SUCCESS:
			return {
				...state,
				isLoading: false,
				data: action.payload.data,
				info: action.payload.info,
				periodData: getDataByPeriod(action.payload.data.find(item => item.label === VIEW_TYPES[0]), PERIODS[1])
			};

		case CHART_GET_DATA_FAILED:
			return {
				...state,
				isLoading: false
			};

		case CHANGE_VIEW_TYPE:
			return {
				...state,
				viewType: action.payload,
				periodData: getDataByPeriod(state.data.find(item => item.label === action.payload), state.period)
			};

		case CHANGE_PERIOD:
			return {
				...state,
				period: action.payload,
				periodData: getDataByPeriod(state.data.find(item => item.label === state.viewType), action.payload)
			};

		default: {
			return state;
		}
	}
};

export default ChartState;
