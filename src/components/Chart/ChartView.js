import React from 'react';
import * as moment from 'moment';
import {Chart} from 'react-charts';
import {useDispatch, useSelector} from "react-redux";
import Dropdown from 'react-dropdown';
import {VIEW_TYPES, PERIODS, CHANGE_VIEW_TYPE, CHANGE_PERIOD} from './ChartState';
import styled from "styled-components";
import 'react-dropdown/style.css';
import {Button, ButtonGroup} from 'react-bootstrap-buttons';
import 'react-bootstrap-buttons/dist/react-bootstrap-buttons.css';

const StyledDropdown = styled(Dropdown)`
    position: absolute;
    right: 0px;
    bottom: 95px;
    width: 110px;
`;

const ChartView = () => {
	const periodData = useSelector(state => state.chart.periodData);
	const viewType = useSelector(state => state.chart.viewType);
	const period = useSelector(state => state.chart.period);
	const dispatch = useDispatch();

	const periodFormat = React.useCallback((d) => {
		switch (period) {
			case 'Year':
			case 'Max':
				return d;
			case 'Week':
			case 'Month':
			case 'Quarter':
				return moment(d).format('MM.DD');
		}
	}, [period]);

	const showTicks = React.useMemo(() => {
		return period === 'Week' || period === 'Month' || period === 'Quarter';
	}, [period]);

	const axes = React.useMemo(
		() => [
			{primary: true, type: 'ordinal', position: 'bottom', format: periodFormat, showTicks},
			{type: 'linear', position: 'left'}
		],
		[period]
	);

	const getSeriesStyle = React.useCallback((series) => {
		const result = {};
		result.color = '#bbb';
		return result;
	}, []);

	const options = React.useMemo(() => {
		return VIEW_TYPES.map(item => {
			return {
				value: item,
				label: item
			}
		})
	}, []);

	const containerStyle = React.useMemo(() => {
		return {
			width: '450px',
			height: '300px',
			position: 'relative',
			marginTop: '20px'
		};
	}, []);


	return (
		<div>
			<ButtonGroup>
				{
					PERIODS.map(item => {
						return <Button key={item}
									   onClick={() => {
										   dispatch({
											   type: CHANGE_PERIOD,
											   payload: item
										   })
									   }}
									   active={period === item}>{item}</Button>
					})
				}
			</ButtonGroup>
			<div style={containerStyle}>
				<Chart data={periodData}
					   axes={axes}
					   getSeriesStyle={getSeriesStyle}
					   tooltip
				/>
				<StyledDropdown options={options}
								onChange={(e) => {
									dispatch({
										type: CHANGE_VIEW_TYPE,
										payload: e.value
									})
								}}
								value={viewType}/>;
			</div>
		</div>
	);
};

export default ChartView;
