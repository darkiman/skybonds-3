import { combineReducers } from 'redux';
import ChartState from '../components/Chart/ChartState';

const rootReducer = combineReducers({
	chart: ChartState,
});

export default rootReducer;
