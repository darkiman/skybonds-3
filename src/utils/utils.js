import * as moment from 'moment';
const YEAR_FORMAT = 'YYYY';
const CURRENT_YEAR = moment().format(YEAR_FORMAT);
const DATE_FORMAT = 'YYYY.MM.DD';

export const getRandom = (min, max) => {
	return parseFloat((Math.random() * (max - min) + min).toFixed(2));
};

export const getRandomData = () => {
	let result = [];
	const startDate = moment().year(CURRENT_YEAR - 1).startOf('year');
	const endDate = moment();
	const monthsDiff = endDate.diff(startDate, 'months');
	for (let i = 0; i < monthsDiff + 1; i++) {
		for (let j = 0; j < 8; j++) {
			const date = moment(startDate).add(i, 'M').add(getRandom(j * 3 , (j * 3) + 3) ,'d').format(DATE_FORMAT);
			const alreadyExist = result.find(item => item.x === date);
			if (!alreadyExist) {
				result.push({
					x: date,
					y: getRandom(1, 500)
				});
			}
		}
	}
	console.log(result);
	return result;
};
