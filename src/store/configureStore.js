import { createStore, applyMiddleware } from 'redux';
import rootReducer from '../reducers/index';
import thunk from 'redux-thunk';

const configureStore = (initialState) => {
	const middleware = applyMiddleware(thunk);
	return createStore(rootReducer, initialState, middleware);
};

export default configureStore;
